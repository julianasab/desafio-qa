Funcionalidade: Gerenciamento de mensagens de uma conversa
	A fim de gerenciar minhas mensagens dentro de uma conversa
	Como usuário do sistema whatsapp
	Eu quero poder apagar, favoritar, desfavoritar, copiar, encaminhar ou referenciar mensagens
	
Contexto:
	Dado que eu esteja cadastrado no whatsapp 
	    E que eu possua um contato do whatsapp cadastrado na minha lista de contatos do celular 
	    E que eu tenha acessado o aplicativo whatsapp no celular
        E que uma mensagem tenha sido enviada
	    E que eu tenha pressionado a mensagem na tela
	    E que o menu superior de gerenciamento de mensagens tenha sido exibido
	
Cenário: Encaminhar uma mensagem de texto
	Quando eu clicar no ícone de encaminhar
	    E selecionar um contato 
	    E clicar no ícone de envio
	Então a mensagem é exibida no histórico de conversas 
	    E o horário de envio é exibido
	    E o status da mensagem é exibido
	
Cenário: Copiar uma mensagem
	Quando eu clicar no ícone de copiar
	Então é exibida a mensagem "Mensagem copiada" 
	    E o conteúdo da mensagem é copiado 
	
Cenário: Referenciar mensagem enviada dentro de uma nova mensagem
	Quando eu clicar no ícone de referenciar
        E a mensagem referenciada for exibida em destaque acima da caixa de diálogo
        E digitar uma mensagem na caixa de diálogo
        E clicar no botão de enviar
	Então a mensagem referenciada é exibida em destaque com o nome do remetente no histórico de conversa 
	    E a mensagem digitada na caixa de diálogo é exibida abaixo da mensagem referenciada
        E o horário de envio é exibido
	    E o status da mensagem é exibido
	
Cenário: Favoritar uma mensagem 
	Quando eu clicar no ícone de favoritar
	Então um ícone de favorito é exibido no final da mensagem no histórico de conversas
	    E a mensagem é exibida na lista de Mensagens Marcadas
	
Cenário: Remover mensagem da lista de favoritos
	Quando eu clicar no ícone de desfavoritar
	Então o ícone de favorito é removido do final da mensagem no histórico de conversas
	    E a mensagem é removida da lista de Mensagens Marcadas
    
Cenário: Apagar uma mensagem 
	Quando eu clicar no ícone de exclusão
	    E clicar na opção "Apagar" da janela de confirmação
	Então é exibida a mensagem "Mensagem apagada" 
	    E a mensagem não é mais exibida no histórico de mensagens