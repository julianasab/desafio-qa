Funcionalidade: Envio de mensagens
	A fim de me comunicar com outro usuário de whatsapp da minha lista de contatos
	Como usuário do whatsapp
	Quero poder enviar mensagens com texto, voz, imagens, coordenadas geográficas, arquivos ou contatos	
		
Contexto:
	Dado que eu esteja logado no whatsapp 
	    E que eu possua um contato do whatsapp cadastrado na minha lista de contatos do celular 
        E que eu esteja na tela de Conversas
	    E que eu tenha clicado no ícone de mensagem no topo da tela	
	    E que eu tenha clicado em um contato da lista
	    E a tela de conversa com o contato esteja aberta
	
Cenário: Enviar mensagem de texto	    
    Quando eu digito a mensagem na caixa de diálogo
        E eu clico no ícone de envio de mensagem
    Então a mensagem é exibida no histórico de conversas 
        E o horário de envio é exibido
        E o status da mensagem é exibido
	
Cenário: Enviar mensagem sem digitar texto 
    Quando eu não digito a mensagem na caixa de diálogo
	Então o ícone de envio de mensagens não é exibido
	    E é exibida a opção de envio de mensagem de voz ao lado da caixa de diálogo

Cenário: Digitar mensagem e envia-la após retornar da lista de conversas
	Dado que eu digite a mensagem na caixa de diálogo
	    E retorne à tela de conversas
	Quando eu clico na conversa novamente
	Então a mensagem continua sendo exibida na caixa de diálogo

Cenário: Enviar mensagem de voz
	Quando eu clico na opção de envio de mensagem de voz 
	    E a mensagem "Pressione e segure para gravar, solte para enviar" é exibida
	    E eu seguro a opção de envio de mensagem de voz
	    E o audio começa a ser gravado
	    E o tempo de gravação é contabilizado
	    E eu paro de segurar a opção de envio de mensagem
	Então a gravação do audio é finalizada
	    E é exibida no histórico de conversas 
	    E o horário de envio é exibido
	    E o status da mensagem é exibido
	
Cenário: Cancelar envio de mensagem de voz
	Quando eu clico na opção de envio de mensagem de voz 
	    E eu seguro a opção de envio de mensagem de voz
	    E o audio começa a ser gravado 
	    E a mensagem de "Deslize para cancelar" é exibida
	    E eu deslizo o dedo para a esquerda 
	Então a gravação do audio é cancelada
	    E a mensagem de audio não é exibida no histórico de mensagem