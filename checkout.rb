class Rule
  def initialize(ruleName, regularValue, quantityForDiscount, discountedValue)
    @ruleName = ruleName
    @regularValue = regularValue
    @quantityForDiscount = quantityForDiscount
    @discountedValue = discountedValue
  end

  def ruleName
    @ruleName
  end

  def calculateTotal(quantity)
    if(@quantityForDiscount != 0 && @discountedValue != 0)
      withDiscount = (quantity / @quantityForDiscount).floor * @discountedValue
      withoutDiscount = (quantity % @quantityForDiscount) * @regularValue
      return withDiscount + withoutDiscount
    else
      return quantity * @regularValue
    end
  end
end

class Checkout
  def initialize(*rules)
    @rules = rules
    @scanned = []
  end

  def scan(name)
    @scanned.push(name)
  end

  def total()
    groupedValues = Hash.new(0)
    sum = 0

    @scanned.each { | name | groupedValues.store(name, groupedValues[name]+1) }

    groupedValues.each { |name, quantity|
      selectedRule = @rules.find { |rule| rule.ruleName == name }
      sum += selectedRule.calculateTotal(quantity)
    }

    return sum
  end
end

require "test/unit"

class CheckoutTests < Test::Unit::TestCase

  def price(scannedItems)
    checkout = Checkout.new(Rule.new("A",50, 3, 130),
    Rule.new("B",30, 2, 45),
    Rule.new("C",20, 0, 0),
    Rule.new("D",15, 0, 0))

    scannedItems.split(//).each { |item| checkout.scan(item) }
    checkout.total
  end

  def test_totals
    assert_equal(  0, price(""))
    assert_equal( 50, price("A"))
    assert_equal( 80, price("AB"))
    assert_equal(115, price("CDBA"))

    assert_equal(100, price("AA"))
    assert_equal(130, price("AAA"))
    assert_equal(180, price("AAAA"))
    assert_equal(230, price("AAAAA"))
    assert_equal(260, price("AAAAAA"))

    assert_equal(160, price("AAAB"))
    assert_equal(175, price("AAABB"))
    assert_equal(190, price("AAABBD"))
    assert_equal(190, price("DABABA"))
  end

  def test_incremental
    checkout = Checkout.new(Rule.new("A",50, 3, 130),
    Rule.new("B",30, 2, 45),
    Rule.new("C",20, 0, 0),
    Rule.new("D",15, 0, 0))

    assert_equal(  0, checkout.total)
    checkout.scan("A");  assert_equal( 50, checkout.total)
    checkout.scan("B");  assert_equal( 80, checkout.total)
    checkout.scan("A");  assert_equal(130, checkout.total)
    checkout.scan("A");  assert_equal(160, checkout.total)
    checkout.scan("B");  assert_equal(175, checkout.total)
  end
end
